pragma ton-solidity >= 0.36.0;
pragma AbiHeader expire;
pragma AbiHeader time;
pragma AbiHeader pubkey;

import "./interfaces/INFTItem.sol";
import "./NFTWallet.sol";

contract NFTAuction {
    address static root_address;
    TvmCell static code_wallet;
    address public deployer;
    string public description;

    bool public canceled;
    address public owner;
    uint128 public startPrice;
    bool public ended;
    bool public ownerHasWithdrawn;
    uint public duration;
    uint public auctionEndTime;
    address public tokenAddress;
    mapping(address => uint128) public fundsByBidder;

    uint8 error_auction_not_ended = 101;
    uint8 error_auction_already_ended = 102;
    uint8 error_already_higher_bid = 103;
    uint8 error_invalid_sender = 104;
    uint8 error_wallet_is_not_valid = 105;
    
    struct Bid {
        uint key;
        uint128 value;
        address bidder;
    }

    Bid public highestBid;

    constructor(address _owner, uint128 _startPrice, uint _duration, address _tokenAddress) public {
        tvm.accept();
        owner = _owner;
        startPrice = _startPrice;
        duration = _duration;
        auctionEndTime = now + _duration;
        tokenAddress = _tokenAddress;
    }

    event bidRefund(address bidder, uint128 value);
    event bidNotAccepted(address bidder, uint128 value);
    event bidPlaced(address bidder, uint128 value);
    event AuctionFinished(address, uint128, address);

    function placeBid() internal {
        require(msg.sender.value != 0, error_invalid_sender);
        require(getExpectedWalletAddress(msg.pubkey(), msg.sender) == msg.sender, error_wallet_is_not_valid);
        require(
            now <= auctionEndTime,
            error_auction_already_ended
        );

        // If the bid is not higher, send the
        // money back.
        require(
            msg.value > highestBid.value,
            error_already_higher_bid
        );
        
        Bid newBid = Bid(msg.pubkey(), msg.value, msg.sender);

        // Bet highest bid
        if(newBid.value > highestBid.value && newBid.value >= startPrice){
            highestBid = newBid;
            emit bidPlaced(newBid.bidder, newBid.value);
        }
        else {
            emit bidNotAccepted(newBid.bidder, newBid.value);
            address(newBid.bidder).transfer(newBid.value, false);
        }
    }
 
    /*
    end auction when time auctionEndTime
    */
    function end() public 
    onlyOwner 
    onlyNotCanceled 
    onlyBeforeEnd {
        require(now >= auctionEndTime, error_auction_not_ended);
        tvm.accept();
        ended = true;
        transferToken(highestBid.bidder);
        emit AuctionFinished(highestBid.bidder, highestBid.value, msg.sender);
    }

    /*
    withdraw seller money after auction finished
    */
    function withdraw() onlyEndedOrCanceled public returns (bool) {
        uint128 value;
        address withdrawalAccount;

        tvm.accept();

        if (canceled) {
            // if the auction was canceled, everyone should simply be allowed to withdraw their funds
            withdrawalAccount  = msg.sender;
            value = fundsByBidder[withdrawalAccount];
        } else {
            // the auction finished without being canceled
            if (msg.sender == owner) {
                // the auction's owner should be allowed to withdraw the highestBindingBid
                withdrawalAccount = highestBid.bidder;
                value = highestBid.value;
                ownerHasWithdrawn = true;
            } else if (msg.sender == highestBid.bidder) {
                revert();
            } else {
                // anyone who participated but did not win the auction should be allowed to withdraw
                // the full value of their funds
                withdrawalAccount = msg.sender;
                value = fundsByBidder[withdrawalAccount];
            }
        }
        
        if (value == 0) revert();

        fundsByBidder[withdrawalAccount] -= value;

        msg.sender.transfer(value);

        return true;
    }

    function cancelAuction() public
        onlyOwner
        onlyBeforeEnd
        onlyNotCanceled
        returns (bool success)
    {
        canceled = true;

        INFTItem(tokenAddress).auctionCancelled();

        return true;
    }

    function transferToken(address recipient) private {
        INFTItem(tokenAddress).auctionEnded(recipient);
    }

    function getTimeRemaining() public view returns (int){
        tvm.accept();
        return int(auctionEndTime) - int(now);
    }

    function getInfo() external view returns
    (
        Bid _currentHighestBid, bool _ended, uint128 _startPrice, string _description, uint _duration, uint _auctionEndTime, int _timeToEnd
    ) {
        _currentHighestBid = highestBid;
        _ended = ended;
        _startPrice = startPrice;
        _description = description;
        _duration = duration;
        _auctionEndTime = auctionEndTime;
        _timeToEnd = int(auctionEndTime) - int(now);
    }

    function getExpectedWalletAddress(uint256 wallet_public_key_, address owner_address_) private inline view returns (address)  {
        TvmCell stateInit = tvm.buildStateInit({
            contr: NFTWallet,
            varInit: {
                root_address: root_address,
                code: code_wallet,
                wallet_public_key: wallet_public_key_,
                owner_address: owner_address_
            },
            pubkey: wallet_public_key_,
            code: code_wallet
        });

        return address(tvm.hash(stateInit));
    }

    modifier onlyEndedOrCanceled {
        if (now < auctionEndTime && !canceled) revert();
        _;
    }

    modifier onlyOwner {
        if (msg.sender != owner) revert();
        _;
    }

    modifier onlyAfterStart {
        if (now < auctionEndTime) revert();
        _;
    }

    modifier onlyBeforeEnd {
        if (now > auctionEndTime) revert();
        _;
    }

    modifier onlyNotCanceled {
        if (canceled) revert();
        _;
    }
}