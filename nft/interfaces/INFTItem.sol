pragma ton-solidity >= 0.40.0;
pragma AbiHeader expire;
pragma AbiHeader time;

struct INFTItemInfo {
    string name;
    string description;
    string url;
    string author;
}

interface INFTItem {
    function getInfo() external view returns (INFTItemInfo);

    function transfer(uint256 to_public_key, address to) external;

    function startAuction(uint128 start_price, uint256 duration) external returns (address);
    function auctionEnded(address recipient) external;
    function auctionCancelled() external;
}