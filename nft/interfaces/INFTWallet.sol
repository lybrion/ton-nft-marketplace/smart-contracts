pragma ton-solidity >= 0.40.0;
pragma AbiHeader expire;
pragma AbiHeader time;

import "./AllowanceInfoStructure.sol";

interface INFTWallet is AllowanceInfoStructure {
    struct INFTWalletDetails {
        address root_address;
        TvmCell code;
        uint256 wallet_public_key;
        address owner_address;
    }

    function getDetails() external view returns (INFTWalletDetails);
}