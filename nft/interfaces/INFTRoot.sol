pragma ton-solidity >= 0.40.0;
pragma AbiHeader expire;
pragma AbiHeader time;

import "./INFTItem.sol";

interface INFTRoot {
    struct INFTRootDetails {
        TvmCell wallet_code;
        TvmCell item_code;
        uint256 root_public_key;
        address root_owner_address;
        uint128 start_gas_balance;
    }

    function getDetails() external view returns (INFTRootDetails);

    function getWalletAddress(uint256 wallet_public_key, address owner_address) external returns (address);

    function deployWallet(
        uint128 grams,
        uint256 wallet_public_key,
        address owner_address,
        address gas_back_address
    ) external returns (address);

    /*function deployEmptyWallet(
        uint128 grams,
        uint256 wallet_public_key,
        address owner_address,
        address gas_back_address
    ) external;*/

    function mint(
        INFTItemInfo token, 
        uint128 grams,
        uint256 wallet_public_key_, address wallet_address_,
        uint256 item_public_key_, address item_address_
    ) external;

    //function withdrawExtraGas() external;
}