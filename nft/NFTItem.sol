pragma ton-solidity >= 0.40.0;
pragma AbiHeader expire;
pragma AbiHeader time;

import "./interfaces/INFTItem.sol";
import "./NFTWallet.sol";
import "./NFTAuction.sol";

contract NFTItem is INFTItem {
    address static root_address;
    INFTItemInfo static info;
    TvmCell static code_wallet;
    uint256 wallet_public_key;
    address owner_address;

    uint128 public target_gas_balance = 0.1 ton;

    optional(address) auction;

    uint8 error_message_sender_is_not_my_owner            = 100;
    uint8 error_not_enough_balance                        = 101;
    uint8 error_message_sender_is_not_my_root             = 102;
    uint8 error_message_sender_is_not_good_wallet         = 103;
    uint8 error_low_message_value                         = 110;

    constructor(uint256 wallet_public_key_, address owner_address_) public {
        wallet_public_key = wallet_public_key_;
        owner_address = owner_address_;
    }

    function getInfo() override external view returns (INFTItemInfo){
        return info;
    }

    function transfer(
        uint256 to_public_key,
        address to
    ) override external onlyOwner noAuction {
        TvmBuilder builder;
        _transfer(to_public_key, to, false, builder.toCell());
    }

    function _transfer(
        uint256 to_public_key,
        address to,
        bool notify_receiver,
        TvmCell payload
    ) private inline {
        require(to.value != 0);

        uint128 reserve = math.max(target_gas_balance, address(this).balance - msg.value);
        require(address(this).balance > reserve + target_gas_balance, error_low_message_value);
        tvm.rawReserve(reserve, 2);

        wallet_public_key = to_public_key;
        owner_address = to;
    }

    function getExpectedWalletAddress(uint256 wallet_public_key_, address owner_address_) private inline view returns (address)  {
        TvmCell stateInit = tvm.buildStateInit({
            contr: NFTWallet,
            varInit: {
                root_address: root_address,
                code: code_wallet,
                wallet_public_key: wallet_public_key_,
                owner_address: owner_address_
            },
            pubkey: wallet_public_key_,
            code: code_wallet
        });

        return address(tvm.hash(stateInit));
    }

    function startAuction(uint128 start_price, uint256 duration) override external onlyOwner noAuction returns (address) {
        auction.set(new NFTAuction(owner_address, start_price, duration, address(this)));

        return auction.get();
    }

    function auctionEnded(address recipient) override external onlyAuction {
        wallet_public_key = 0;
        owner_address = recipient;

        auction.reset();
    }

    function auctionCancelled() override external onlyAuction {
        auction.reset();
    }

    modifier onlyRoot() {
        require(isRoot(), error_message_sender_is_not_my_root);
        _;
    }

    modifier onlyOwner() {
        require(isOwner(), error_message_sender_is_not_my_owner);
        _;
    }

    modifier onlyInternalOwner() {
        require(isInternalOwner());
        _;
    }

    modifier onlyAuction() {
        require(isAuction());
        _;
    }

    modifier noAuction() {
        require(!auction.hasValue());
        _;
    }

    function isRoot() private inline view returns (bool) {
        return root_address == msg.sender;
    }

    function isOwner() private inline view returns (bool) {
        return isInternalOwner() || isExternalOwner();
    }

    function isInternalOwner() private inline view returns (bool) {
        return owner_address.value != 0 && owner_address == msg.sender;
    }

    function isExternalOwner() private inline view returns (bool) {
        return wallet_public_key != 0 && wallet_public_key == tvm.pubkey();
    }

    function isAuction() private inline view returns (bool) {
        return auction.hasValue() && auction.get() == msg.sender;
    }
}