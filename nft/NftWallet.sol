pragma ton-solidity >= 0.40.0;
pragma AbiHeader expire;
pragma AbiHeader time;

import "./interfaces/INFTWallet.sol";
import "./interfaces/INFTItem.sol";

contract NFTWallet is INFTWallet {
    address static root_address;
    TvmCell static code;
    //for external owner
    uint256 static wallet_public_key;
    //for internal owner
    address static owner_address;

    uint8 error_message_sender_is_not_my_owner            = 100;
    uint8 error_not_enough_balance                        = 101;
    uint8 error_message_sender_is_not_my_root             = 102;
    uint8 error_message_sender_is_not_good_wallet         = 103;
    uint8 error_wrong_bounced_header                      = 104;
    uint8 error_wrong_bounced_args                        = 105;
    uint8 error_non_zero_remaining                        = 106;
    uint8 error_no_allowance_set                          = 107;
    uint8 error_wrong_spender                             = 108;
    uint8 error_not_enough_allowance                      = 109;
    uint8 error_low_message_value                         = 110;
    uint8 error_define_wallet_public_key_or_owner_address = 111;

    uint128 public target_gas_balance                      = 0.1 ton;

    constructor() public {
        require((wallet_public_key != 0 && owner_address.value == 0) ||
                (wallet_public_key == 0 && owner_address.value != 0),
                error_define_wallet_public_key_or_owner_address);
        tvm.accept();
    }

    function getDetails() override external view returns (INFTWalletDetails){
        return INFTWalletDetails(
            root_address,
            code,
            wallet_public_key,
            owner_address
        );
    }

    modifier onlyRoot() {
        require(isRoot(), error_message_sender_is_not_my_root);
        _;
    }

    modifier onlyOwner() {
        require(isOwner(), error_message_sender_is_not_my_owner);
        _;
    }

    modifier onlyInternalOwner() {
        require(isInternalOwner());
        _;
    }

    function isRoot() private inline view returns (bool) {
        return root_address == msg.sender;
    }

    function isOwner() private inline view returns (bool) {
        return isInternalOwner() || isExternalOwner();
    }

    function isInternalOwner() private inline view returns (bool) {
        return owner_address.value != 0 && owner_address == msg.sender;
    }

    function isExternalOwner() private inline view returns (bool) {
        return wallet_public_key != 0 && wallet_public_key == tvm.pubkey();
    }
}