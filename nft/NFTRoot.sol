pragma ton-solidity >= 0.40.0;
pragma AbiHeader expire;
pragma AbiHeader time;

import "./interfaces/INFTRoot.sol";
import "./interfaces/INFTItem.sol";
import "./NFTWallet.sol";
import "./NFTItem.sol";

contract NFTRoot is INFTRoot {
    TvmCell public static wallet_code;
    TvmCell public static item_code;
    uint128 public start_gas_balance = 1 ton;
    uint128 public total_supply;
    uint256 static root_public_key;
    address static root_owner_address;

    uint8 error_message_sender_is_not_my_owner = 100;
    uint8 error_not_enough_balance = 101;
    uint8 error_message_sender_is_not_good_wallet = 103;
    uint8 error_define_wallet_public_key_or_owner_address = 106;
    uint8 error_incorrect_wallet_owner_address = 107;
    uint8 error_define_item_public_key_or_owner_address = 108;

    constructor(TvmCell wallet_code_, TvmCell item_code_, uint256 root_public_key_, address root_owner_address_) public {
        require((root_public_key_ != 0 && root_owner_address_.value == 0) ||
                (root_public_key_ == 0 && root_owner_address_.value != 0),
                1033);
        tvm.accept();

        wallet_code = wallet_code_;
        item_code = item_code_;
        root_public_key = root_public_key_;
        root_owner_address = root_owner_address_;
    }

    function getDetails() override external view returns (INFTRootDetails) {
        return INFTRootDetails(
            wallet_code,
            item_code,
            root_public_key,
            root_owner_address,
            start_gas_balance
        );
    }

    function getWalletAddress(uint256 wallet_public_key_, address owner_address_) override external returns (address) {
        require((owner_address_.value != 0 && wallet_public_key_ == 0) ||
                (owner_address_.value == 0 && wallet_public_key_ != 0),
                105);
        address walletAddress = getExpectedWalletAddress(wallet_public_key_, owner_address_);

        return walletAddress;
    }

    function getExpectedWalletAddress(uint256 wallet_public_key_, address owner_address_) private inline view returns (address)  {
        TvmCell stateInit = tvm.buildStateInit({
            contr: NFTWallet,
            varInit: {
                root_address: address(this),
                code: wallet_code,
                wallet_public_key: wallet_public_key_,
                owner_address: owner_address_
            },
            pubkey: wallet_public_key_,
            code: wallet_code
        });

        return address(tvm.hash(stateInit));
    }

    function deployWallet(
        uint128 grams,
        uint256 wallet_public_key_,
        address owner_address_,
        address gas_back_address
    ) override external onlyOwner returns (address) {
        require((owner_address_.value != 0 && wallet_public_key_ == 0) ||
                (owner_address_.value == 0 && wallet_public_key_ != 0),
                error_define_wallet_public_key_or_owner_address);

        if(root_owner_address.value == 0) {
            tvm.accept();
        } else {
            tvm.rawReserve(math.max(start_gas_balance, address(this).balance - msg.value), 2);
        }

        address wallet = new NFTWallet{
            value: grams,
            code: wallet_code,
            pubkey: wallet_public_key_,
            varInit: {
                root_address: address(this),
                code: wallet_code,
                wallet_public_key: wallet_public_key_,
                owner_address: owner_address_
            }
        }();

        if (root_owner_address.value != 0) {
            if (gas_back_address.value != 0) {
                gas_back_address.transfer({ value: 0, flag: 128 }); 
            } else {
                msg.sender.transfer({ value: 0, flag: 128 }); 
            }
        }

        return wallet;
    }

    function mint(
        INFTItemInfo token, 
        uint128 grams,
        uint256 wallet_public_key_, address wallet_address_,
        uint256 item_public_key_, address item_address_
    ) override external onlyOwner {
        require((wallet_address_.value != 0 && wallet_public_key_ == 0) ||
                (wallet_address_.value == 0 && wallet_public_key_ != 0),
                error_incorrect_wallet_owner_address);
        require((item_address_.value != 0 && item_public_key_ == 0) ||
                (item_address_.value == 0 && item_public_key_ != 0),
                error_define_item_public_key_or_owner_address);
        require(getExpectedWalletAddress(wallet_public_key_, wallet_address_) == item_address_, error_incorrect_wallet_owner_address);

        address item = new NFTItem{
            value: grams,
            code: item_code,
            pubkey: wallet_public_key_,
            varInit: {
                root_address: address(this),
                code_wallet: wallet_code,
                info: token
            }
        }(wallet_public_key_, wallet_address_);

        if(root_owner_address.value != 0) {
            root_owner_address.transfer({ value: 0, flag: 128 }); 
        }
    }

    modifier onlyOwner() {
        require(isOwner(), 1001);
        _;
    }

    modifier onlyInternalOwner() {
        require(isInternalOwner(), 102);
        _;
    }

    function isOwner() private inline view returns (bool) {
        return isInternalOwner() || isExternalOwner();
    }

    function isInternalOwner() private inline view returns (bool) {
        return root_owner_address.value != 0 && root_owner_address == msg.sender;
    }

    function isExternalOwner() private inline view returns (bool) {
        return root_public_key != 0 && root_public_key == msg.pubkey();
    }
}